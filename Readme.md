# Basic OTP app

A python script that implements **Yandex.Key**'s OTP method, along with Steam's and normal.

---
Usage: `ya_key` to generate yandex OTP. You will be prompted for secret (in base32 form) interactively, and for the password.
If you don't want to type in password all the time, you can concat your password with binary form of your secret, hash this with SHA256 and use this as secret.
App does not require any internet connection. It does not store any data.


Please note that no actual security features were kept in mind when developing this.
By using this, you accept all risks of doing anything with this app.

---
All python code is licensed under `GNU AGPL-3.0`.

