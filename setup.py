from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='yandex_key',
      version='0.1',
      description='A TOTP generator for YandexKey',
      author='Saphire Lattice',
      packages=['yandex_key'],
      package_dir={'yandex_key': 'yandex_key'},
      scripts=['bin/yakey'],
      install_requires=requirements)
