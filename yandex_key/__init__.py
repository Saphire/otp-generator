import hashlib
import hmac
import base64
import struct


class Factory:
    def __init__(self, key: str, salt=None, otp_algorithm=None, salt_algorithm=None,
                 charset: str = None, length: int = None, byte_format: str = None, byte_mask: int = None,
                 reverse: bool = None):
        self.otp_algorithm = otp_algorithm
        self.salt_algorithm = salt_algorithm
        self.charset = charset
        self.length = length
        self.byte_format = byte_format
        self.byte_mask = byte_mask
        self.reverse = reverse

        key = self.base32_decode(key)
        if salt is not None:
            key = self.salt(key, salt)
        self.key = key

    def base32_decode(self, data: str) -> bytes:
        data = data.upper()
        base32_charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        for char in data:
            if char not in base32_charset:
                data = data.replace(char, "")
        data += ("=" * ((8 - len(data) % 8) % 8))
        return base64.b32decode(data)

    def salt(self, key: bytes, salt_data: str):
        salted = bytes(salt_data, "UTF-8") + key
        if salted[0] == 0:
            print("TODO: SHIFT SECRET IF FIRST BYTE IS 0")
        return self.salt_algorithm(salted).digest()

    def charset_encode(self, data: int, output_length: int):
        charset_length = len(self.charset)
        data = data % pow(charset_length, output_length)
        result = ""

        for i in range(output_length):
            pos = data % charset_length
            ch = self.charset[pos:pos + 1]
            if self.reverse is True:
                result = ch + result
            else:
                result += ch
            data = int(data / charset_length)

        return result[::-1]

    def generate(self, counter: int, salt: str = None):
        key = self.key
        if salt is not None:
            key = self.salt(key, salt)

        data = bytearray(8)
        struct.pack_into(">q", data, 0, counter)

        out = hmac.new(key, data, digestmod=self.otp_algorithm).digest()
        offset = out[len(out) - 1] & 0x0F
        out_long = (struct.unpack_from(self.byte_format, out, offset)[0] & self.byte_mask)

        return self.charset_encode(out_long, self.length)
