class OtpType:
    def __init__(self, name: str, length: int, algorithm: str = 'sha1',
                 format: str = '>l', format_mask = 0x7FFFFFFF,
                 charset: str = '0123456789', period: int = 30, reverse: bool = False):
        self.name = name
        self.length = length
        self.algorithm = algorithm
        self.format = format
        self.format_mask = format_mask
        self.charset = charset
        self.period = period
        self.reverse = reverse

named = {
    'yandex': OtpType('yaotp', 8, 'sha256', '>q', 0x7FFF_FFFF_FFFF_FFFF, 'abcdefghijklmnopqrstuvwxyz'),
    'steam': OtpType('steam', 5, 'sha1', charset='23456789BCDFGHJKMNPQRTVWXY', reverse=True),
    'totp': OtpType('totp', 6, 'sha1')
}
